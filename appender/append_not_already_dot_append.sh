#!/bin/sh
#   Copyright 2013 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# The first argument is the target file
# The second argument is the line which needs appending and should
# not begin with comment character (#)

[ $# -lt 2 ] && exit 101
FILE_=$1
EGREP_LINE_=$2

[ ! -r ${FILE_} ] && exit 102

#printf "Append request for file: $FILE_\n"
#printf "To egrep: $EGREP_LINE_\n"

GOT_=$(/bin/egrep "${EGREP_LINE_}" "${FILE_}")
GOT_RC_=$?
#printf "${GOT_RC_}\n"

if [ "${GOT_RC_}" -eq 0 ]; then
    # grep found egrep_line
    exit 0
fi

if [ "${GOT_RC_}" -eq 1 ]; then
    # grep did not find egrep_line
    #printf "Appending...\n"
    if [ -r "${FILE_}.append" ]; then
	/bin/cp -np "${FILE_}" "${FILE_}.original"
	#printf "${FILE_} ${FILE_}.append\n"
	CAT_=$(/bin/cat "${FILE_}" "${FILE_}.append" > "${FILE_}.combined")
	CAT_RC_=$?
	# Avoid trying to fight with the safety features of cat ...
	# CAT_=$(/bin/cat ${FILE_} | /bin/cat - "${FILE_}.append" > "${FILE_}.combined")
	# ... such as message 'input file is output file'
	/bin/cp -p --backup "${FILE_}.combined" "${FILE_}"
	# Above I have manually done that which cat is designed to do - namely
	# appending to a file without obliterating it on disk.
	# If you feel that there is no benefit from keeping a copy in .combined
	# then do use cat >> feature as it is superior in most respects.
	[ "${CAT_RC_}" -eq 0 ] && exit 0
    else
	exit 120
    fi
else
    # grep return code neither 0 (found) nor 1 (not found)
    exit 121
fi
exit 125
