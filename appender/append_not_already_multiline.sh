#!/bin/sh
#   Copyright 2013 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# The first argument is the target file
# The second argument is the line which needs appending and should
# not begin with comment character (#)

[ $# -lt 2 ] && exit 101
APPEND_FILE_=$1
APPEND_LINE_=$2

[ ! -r ${APPEND_FILE_} ] && exit 102

#printf "Append request for file: $APPEND_FILE_\n"
#printf "To append: $APPEND_LINE_\n"

GOT_=$(/bin/grep "${APPEND_LINE_}" "${APPEND_FILE_}")
GOT_RC_=$?
#printf "${GOT_RC_}\n"

if [ "${GOT_RC_}" -eq 0 ]; then
    # grep found append_line
    exit 0
fi

if [ "${GOT_RC_}" -eq 1 ]; then
    # grep did not find append_line
    #printf "Appending...\n"
    printf "${APPEND_LINE_}\n" >> "${APPEND_FILE_}" 
else
    # grep return code neither 0 (found) nor 1 (not found)
    exit 121
fi
