#!/bin/dash
#   Copyright 2013 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# This is a helper script intended for su as user ...
# where that user is NOT root
# There are existing mechanisms in sudo and direct calling of su to support
# typical su as root. My intention is not to duplicate them here.
# Uid must be greater than 30 otherwise su will not be invoked.

[ $# -lt 2 ] && exit 101
USER_=$1
CMD_FIRST_WORD_=$2
# Next we lose the user argument so not included in $* below
shift
# Put any sanitisation on COMMAND_ here
COMMAND_REPEAT_=$*
#printf "$COMMAND_REPEAT_\n"

su_flag=1
exit_rc=126
UID_=$(/usr/bin/id -u ${USER_})
RC_=$?
if [ ${RC_} -eq 0 ]; then
    exit_rc=105
    if [ ${UID_} -lt 31 ]; then
	su_flag=0
	exit_rc=121
    fi
else
    su_flag=0
    exit_rc=111
fi

if [ $su_flag -gt 0 ]; then
    exit_rc=0
    #printf "Running as user $USER_ the command $COMMAND_REPEAT_\n"
else
    printf "NOT Running as user $USER_ having uid=$UID_ the command $COMMAND_REPEAT_\n"
    exit $exit_rc
fi

/bin/su ${USER_} --shell=/bin/dash -c "${COMMAND_REPEAT_}"
exit $?
