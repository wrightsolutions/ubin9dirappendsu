#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#from subprocess import Popen,PIPE
import subprocess
import shlex
import re
import argparse
from sys import exit
from os import sep as ossep
from os import getcwd

def subout_errors():
	global mercurial_files_differ, mercurial_only_in
	global git_files_differ, git_only_in

	subout_errors_rc = 0
	for line in subout:
		if re_mercurial.search(line):
			if re_only_in.match(line):
				mercurial_only_in += 1
			elif re_files_differ.match(line):
				mercurial_files_differ += 1
			continue
		elif re_git.search(line):
			if re_only_in.match(line):
				git_only_in += 1
			elif re_files_differ.match(line):
				git_files_differ += 1
			continue
		elif re_only_in.match(line):
			if re_mercurial_colon.search(line):
				mercurial_only_in += 1
				continue
			elif re_mercurial_colon.search(line):
				git_only_in += 1
				continue
		elif re_of_utf8_data.search(line):
			#print ".search(%s)" % line
			subout_errors_rc = 111
			break
	        """ not interested in lines that match supplied globignore 
		# re_globignore.match(line) can be remembered as egrep '^needle'
	        if re_globignore.search(line):
	                continue
		"""
		print line.rstrip()
	return subout_errors_rc

parser = argparse.ArgumentParser()
parser.add_argument('newdir', default=getcwd(), help=
		    'Directory: latest [new] version (default: current working directory)')
parser.add_argument('olddir',
		    help='Directory: previous [old] version (no default)')
args = parser.parse_args()

re_mercurial = re.compile("{0}.hg{0}".format(ossep,ossep))
re_mercurial_colon = re.compile("{0}.hg:".format(ossep))
re_git = re.compile("{0}.git{0}".format(ossep,ossep))
re_git_colon = re.compile("{0}.git:".format(ossep))
""" The next regular expression is not something I would expect to encounter,
but serves to illustrate how you would go about testing for error output from
the actual diff command. """
re_of_utf8_data = re.compile("of UTF8 data")

re_only_in = re.compile("Only in")
re_files_differ = re.compile("Files\s.*\sdiffer")

mercurial_files_differ = 0
mercurial_only_in = 0
git_files_differ = 0
git_only_in = 0

diffcmd = "/usr/bin/diff -r --brief {0} {1}".format(args.olddir,args.newdir)
print diffcmd
subout = (subprocess.Popen(shlex.split(diffcmd),bufsize=16384, \
		stdout=subprocess.PIPE,stderr=subprocess.STDOUT)).stdout
subout_rc = subout_errors()

if mercurial_files_differ or mercurial_only_in:
	print "also /.hg/ Files 'Differ' count is {0}".format(mercurial_files_differ)
	print "also /.hg/ Files 'Only in' count is {0}".format(mercurial_only_in)
elif git_files_differ or git_only_in:
	print "also /.git/ Files 'Differ' count is {0}".format(git_files_differ)
	print "also /.git/ Files 'Only in' {0}".format(git_only_in)

if subout_rc > 0:
	print "Warning: errors experienced during diff..."
	if subout_rc > 0:
		exit(subout_rc)
	exit(101)

