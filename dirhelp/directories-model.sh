#!/bin/sh
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Print directories permissions and ownership for
# all directories below SITEROOT_ except those indicated by EXCLUDE_

# ./directories-model.sh ~/sites
# ./directories-model.sh ~/sites 'default/hello'

SITEROOT_='/var/www/vhosts/dynamic'
EXCLUDE_='default/files'
if [ $# -gt 0 ]; then
    if [ ! -z "$1" ]; then
                SITEROOT_=$1
		if [ $# -gt 1 ]; then
		    if [ ! -z "$2" ]; then
	               EXCLUDE_=$2
		    fi
		fi
        fi
fi

#printf "%s\n" "${EXCLUDE_}"
#find ${SITEROOT_} -type d -not -regex ".*/${EXCLUDE_}"
/usr/bin/find ${SITEROOT_} -regex ".*/${EXCLUDE_}" \
    -prune -o -type d -not -regex ".*/${EXCLUDE_}" -exec ls -dlp {} \;
