#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import subprocess
import shlex
import re
import argparse
from sys import exit
from os import sep as ossep
from os import getcwd

def subout_errors():
	global mercurial_count, git_count

	subout_errors_rc = 0
	for line in subout:
		if re_mercurial.match(line):
			mercurial_count += 1
			continue
		elif re_git.match(line):
			git_count += 1
			continue
		elif re_sending_incremental.match(line):
			continue
		elif re_bytes_sec.search(line):
			continue
		elif re_size_speedup.search(line):
			continue
		elif re_io_c.search(line):
			#print ".search(%s)" % line
			subout_errors_rc = 111
			break
	        """ not interested in lines that match supplied globignore 
		# re_globignore.match(line) can be remembered as egrep '^needle'
	        if re_globignore.search(line):
	                continue
		"""
		line = line.rstrip()
		if len(line) > 1:
			print line
	return subout_errors_rc

parser = argparse.ArgumentParser()
parser.add_argument('newdir', default=getcwd(), help=
		    'Directory: latest [new] version (default: current working directory)')
parser.add_argument('olddir',
		    help='Directory: previous [old] version (no default)')
args = parser.parse_args()

re_mercurial = re.compile(".hg{0}".format(ossep,ossep))
re_git = re.compile(".git{0}".format(ossep,ossep))
""" The next regular expression is not something I would expect to encounter,
but serves to illustrate how you would go about testing for error output from
the actual rsync command. """
re_io_c = re.compile("at io.c")

mercurial_count = 0
git_count = 0

olddir = args.olddir
if not olddir.endswith(ossep):
	olddir = args.olddir + ossep
newdir = args.newdir
if not newdir.endswith(ossep):
	newdir = args.newdir + ossep
rsynccmd = "rsync -v -r --dry-run {0} {1}".format(olddir,newdir)
print rsynccmd
subout = (subprocess.Popen(shlex.split(rsynccmd),bufsize=16384, \
		stdout=subprocess.PIPE,stderr=subprocess.STDOUT)).stdout

re_sending_incremental = re.compile("sending\sincremental\s")
re_bytes_sec = re.compile("bytes\s.*\d+.*\sbytes\/sec")
re_size_speedup = re.compile("size\s.*\d+.*\sspeedup")
subout_rc = subout_errors()

if mercurial_count:
	print "also .hg/ Files count is {0}".format(mercurial_count)
elif git_count:
	print "also .git/ Files count is {0}".format(git_count)

if subout_rc > 0:
	print "Warning: errors experienced during rsync..."
	if subout_rc > 0:
		exit(subout_rc)
	exit(101)

